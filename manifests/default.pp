   include jenkins


####

jenkins::plugin {
  "git" :
    version => "1.1.11";
}

jenkins::plugin { "ldap": }
jenkins::plugin {
  "credentials":
    version => "1.16.1"
  }
jenkins::plugin { "ssh-credentials": }
jenkins::plugin { "cas1": }
jenkins::plugin { "email-ext": }
  jenkins::plugin {
    "mailer" :
      version => "1.5"
  }
  jenkins::plugin {
    "token-macro" :
      version => "1.10"
  }
  jenkins::plugin {
    "analysis-core" :
      version => "1.54"
  }
  jenkins::plugin {
    "config-file-provider" :
      version => "2.7.1"
  }

jenkins::plugin { "chucknorris": }


package {"dejavu-fonts":
  ensure => present
}


#The following needed to occur to get Jenkins to boot
#I had to manually accept the token for Jenkins via $ sudo zypper install jenkins and then reprovsion
#zypper install dejavu-fonts, http://stackoverflow.com/questions/8109607/headless-continuous-integration-with-jenkins
#

